from PIL import Image
from bisect import bisect
import random 
import tkinter 
import webbrowser

def conv(img):
	
	img = Image.open(img)
	img = img.convert("L")
	print(img.size[0])
	x = img.size[0]
	y = img.size[1]
	xscale = x//100
	x = x//xscale
	y = y//xscale
	img = img.resize((x,y),Image.BILINEAR)
	#img.show()

	img.format= "png"
	greyscale = [
		    " ",
		    " ",
		    ".,-",
		    "_ivc=!/|\\~",
		    "gjez2]/(YL)t[+T7Vf",
		    "mdK4ZGbNDXY5P*Q",
		    "W8KMA",
		    "#%$"
		    ]
	zonebounds=[36,72,108,144,180,216,252]
	ascii = ""

	for y in range(img.size[1]):
		for x in range(img.size[0]):
			lum = 255-img.getpixel((x,y))
			row = bisect(zonebounds,lum)
			possibles = greyscale[row]
			ascii=ascii+possibles[random.randint(0,len(possibles)-1)]
		ascii=ascii+'\n'
	return(ascii)



def save(text):
	file = open('img.txt','w')
	file.write(text)
	file.close()
def display(f):
	webbrowser.open(f)


tk = tkinter.Tk()
frame = tkinter.Frame(tk)

label = tkinter.Label(frame,text = "ASCII IMG GENERATOR")
label.pack(expand=1)

e = tkinter.Entry(frame, text = "Image Path")
e.pack(expand=1)


gen = tkinter.Button(frame, text = "Generate Ascii", command = lambda: save(conv(e.get())))
gen.pack(expand=1)

disp = tkinter.Button(frame, text = "Open ASCII Art TXT", command = lambda: display('img.txt')).pack()

frame.pack()
tk.mainloop()






	


