Img2ASCII
=========

Image to ascii art converter


Windows
------
Run the executable

Linux/Mac/Etc
-------------
In terminal:

pip install -r reqs.txt 

python ascii.py
